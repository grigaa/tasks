/*
myFun( String ) - Функция принимает на вход строковый параметр с названием идентификатора, класса или тега
			и возвращает либо объект, либо массив объектов, удовлетворяющих условию поиска, в противном случае null, 
			если ничего найдено.
			Перед названием класса ставится символ '.'
			Перед названием идентификатора ставится символ '#'

Методы:
addClass( String ) - Добавляет элементу указанный класс
removeClass( String ) - Удаляет из элемента указанный класс
checkClass ( String ) - Проверяет наличие указанного класса в элементе, возвращает true, 
			если класс найден, в противном случае false
showClasses() - Возвращает массив, состоящий из классов указанного элемента
changeClass( String, String ) - Заменяет класс, переданный первым параметром, на класс, 
			переданный вторым параметром
*/

function myFun(selector) {
	if (typeof(selector) == "string") {
		if (selector.length > 0) {
			if (selector[0] == '#') {
				return document.getElementById(selector.substring(1));
			}
			if (selector[0] == '.') {
				return document.getElementsByClassName(selector.substring(1));
			};
			if ((selector[0] != '#') && (selector[0] != '.')) {
				return document.getElementsByTagName(selector);
			};
		} else {
			return "Empty parametr";
		}
	} else {
		return false;
	}
};

Object.extend = function(self, obj) {
	for (var key in obj) self[key] = obj[key];
	return self;
};

Object.extend(Object.prototype, {
	addClass: function(className) {
		if (className == undefined) {
			return "Use correct: addClass(className)";
		} else {
			this.classList.add(className);
			return this;
		}
	},
	removeClass: function(className) {
		if (className == undefined) {
			return "Use correct: removeClass(className)";
		} else {
			this.classList.remove(className);
			return this;
		}
	},
	checkClass: function(className) {
		if (this.classList.contains(className)) {
			return true;
		} else {
			return false;
		}
	},
	showClasses: function() {
		var arr = [];
		this.classList.forEach(function(elem) {
			arr.push(elem);
		});
		return arr;
	},
	changeClass: function(classNameRm,classNameAdd) {
		if (classNameAdd == undefined || classNameRm == undefined) {
			return "Use correct: changeClasses(classNameRm,classNameAdd)";
		} else {
			this.classList.remove(classNameRm);
			this.classList.add(classNameAdd);
			return this;	
		}				
	},
});