/*

describeRoute( Object, Boolean ) - Функция принимает на вход первым параметром объект, содержащий информацию с карточек,
			и вторым параметром true - в случае, если необходимо получить полное описание маршрута, 
			либо false - если необходимо получить последовательность пересадочных точек на маршруте

Формат входных данных:
	{
		"a":"Точка начала отрезка",
		"b":"Точка окончания отрезка",
		"placeA":"Место начала отрезка",
		"placeB":"Место окончания отрезка",
		"transportType":"Тип транспорта",
		"params":"Параметры указанного типа транспорта"
	}

Формат выходных данных:
	"From 'a' 'placeA' to 'b' 'placeB' at 'transportType' ('params')"


Пример:

var cards = [
				{ "a":"Moscow", "b":"Saint Petersburg", "placeA":"airport", "placeB":"airport", "transportType":"plane", "params":"22,12F" },
				{ "a":"Kislovodsk", "b":"Sochi", "placeA":"bus station", "placeB":"railway station", "transportType":"bus", "params":"152,34" },
				{ "a":"Adler", "b":"Moscow", "placeA":"airport", "placeB":"airport", "transportType":"plane", "params":"3,10C" },
				{ "a":"Saint Petersburg", "b":"Kazan", "placeA":"airport", "placeB":"airport", "transportType":"plane", "params":"1,2A" },
				{ "a":"Sochi", "b":"Adler", "placeA":"railway station", "placeB":"airport", "transportType":"taxi", "params":"Ford Focus,A240NP77,8(999)1234567" },
			];

describeRoute( cards, false );
// "Kislovodsk - Sochi - Adler - Moscow - Saint Petersburg - Kazan"

describeRoute( cards, true );
//"From Kislovodsk bus station to Sochi railway station at bus (number: 152, seat: 34)
//From Sochi railway station to Adler airport at taxi (model: Ford Focus, number: A240NP77, phone: 8(999)1234567)
//From Adler airport to Moscow airport at plane (gate: 3, seat: 10C)
//From Moscow airport to Saint Petersburg airport at plane (gate: 22, seat: 12F)
//From Saint Petersburg airport to Kazan airport at plane (gate: 1, seat: 2A)
//"

*/

function describeRoute (cards, desc) {
	var keys = [];
	var counts = [];
	var values = [];

	var j = 0;

	cards.forEach(function (elem, index) {
		checkCard("a", index);
		checkCard("b", index);
	});

	function checkCard (point, i) {
		if (keys.indexOf(cards[i][point])>=0) {
			counts[keys.indexOf(cards[i][point])]++;
			values[keys.indexOf(cards[i][point])] += point;
		} else {
			keys[j] = cards[i][point];
			counts[j] = 1;
			values[j] = point;
			j++;
		}
	}

	var routeRing = counts.every(function (elem) {
		return elem == 2; 
	});

	var routeLoop = counts.some(function (elem) {
		return elem > 2; 
	});

	if (routeRing || routeLoop) {
		alert("Маршрут не является линейным!");
		return false;
	}

	var start, finish;

	counts.forEach(function (elem, index) {
		if (counts[index] == 1) {
			if (values[index] == "a") {
				start = keys[index];
			}
			if (values[index] == "b") {
				finish = keys[index];
			}
		}
	})

	var path = [];

	lengthPath = cards.length;
	j = 0;
	var curFinish = "";
	var flagStart = true;
	while (j != lengthPath) {
		cards.forEach(function (elem, index) {
			if (cards[index].a == start && flagStart) {
				flagStart = false;
				path[j] = index;
				curFinish = cards[index].b;
				j++;
			}
			if (cards[index].a == curFinish) {
				path[j] = index;
				curFinish = cards[index].b;
				j++;
			}
		})
	}

	var transports = {
						"bus":["number", "seat"],
						"plane":["gate", "seat"],
						"taxi":["model", "number", "phone"],
						"train":["number", "carriage", "seat"],
						"ship":["number", "seat"],
	};

	var strPath = "";

	cards.forEach(function (elem, index) {
		strPath += cards[path[index]].a;
		strPath += " - ";
	})

	strPath += cards[path[path.length - 1]].b;

	var descriptionPath = "";

	cards.forEach(function (elem, index) {
		var params = ""
		paramsName = transports[cards[path[index]].transportType];
		paramsValue = cards[path[index]].params.split(",");
		paramsName.forEach(function (elem, index) {
			if (index != 0) {
				params += (", " + elem + ": " + paramsValue[index]);
			} else {
				params += (elem + ": " + paramsValue[index])
			}
				
		});
		descriptionPath += ("From " + cards[path[index]].a + " " + cards[path[index]].placeA + " to " + cards[path[index]].b + " " + cards[path[index]].placeB+ " at " + cards[path[index]].transportType + " (" + params + ")\n");
	})
	if (desc) {
		return descriptionPath;
	} else {
		return strPath;
	}
}